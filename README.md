# RapidHaskell Workshop 2

This is the output of Workshop 2.

To build

```{.bash}
stack build
stack exec -- ws2-exe
```

Slides are [here](http://rapidhaskell2018.gitlab.io/workshops/2.pdf)
