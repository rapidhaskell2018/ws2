module Main where

import RIO

data Dragon = Dragon {
  name  :: String
, heads :: Int
} deriving (Eq, Show)

instance Semigroup Dragon where
  Dragon n1 h1 <> Dragon n2 h2 =
    Dragon (n1 <> n2) (h1 + h2)

instance Monoid Dragon where
  mempty = Dragon "" 0

main :: IO ()
main = do
  let x = Dragon "Rainbow" 4
  withBinaryFile "log.txt" WriteMode $ \logHandle -> do
   logStdout <- logOptionsHandle stdout True
   logFile <- logOptionsHandle logHandle True
   withLogFunc logStdout $ \lfs ->
    withLogFunc logFile $ \lfe ->
     runRIO (lfs <> lfe) $ do
      logInfo $ "Hello, " <> (fromString . name $ x)
      logInfo "Hello, World!"
      logDebug "Goodbye"
      logWarn "What the..."
      logError "OHNO!"
